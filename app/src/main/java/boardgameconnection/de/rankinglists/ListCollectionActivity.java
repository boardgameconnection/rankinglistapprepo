package boardgameconnection.de.rankinglists;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class ListCollectionActivity extends AppCompatActivity {

    ArrayList<String> lists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_collection);

        setupList();
    }

    private void setupList() {
        lists.add("Board Games");
        lists.add("Video Games");
        lists.add("Restaurants in Essen");
        lists.add("RPGs");
        lists.add("Social Deduction Games");
    }
}
