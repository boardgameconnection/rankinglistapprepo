package boardgameconnection.de.rankinglists;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItem;
import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements ListSwipeHelper.OnSwipeListener, EditDialogFragment.DialogInteractionListener, View.OnClickListener {

    private ArrayList<Pair<Long, String>> itemList;
    private DragListView dragListView;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //TODO delete when app is finished
        setupList();

        setupListView();

        fab = findViewById(R.id.list_fab);
        fab.setOnClickListener(this);
    }

    //UI List setup
    private void setupListView() {
        dragListView = findViewById(R.id.draglist_view);
        dragListView.setVerticalScrollBarEnabled(true);
        dragListView.setSwipeListener(this);
        dragListView.setLayoutManager(new LinearLayoutManager(this));
        DragListAdapter adapter = new DragListAdapter(itemList, R.layout.draglist_item, R.id.image_circle, false);
        dragListView.setAdapter(adapter, false);
        dragListView.setCanDragHorizontally(false);
        dragListView.setCustomDragItem(new MyDragItem(this, R.layout.draglist_item));
        dragListView.getRecyclerView().addOnScrollListener(onScrollListener);

    }

    //TODO delete when app is finished
    //Dummy List for test use
    private void setupList() {
        itemList = new ArrayList<>();
        itemList.add(new Pair<Long, String>((long) 0, "Scythe"));
        itemList.add(new Pair<Long, String>((long) 1, "Gloomhaven"));
        itemList.add(new Pair<Long, String>((long) 2, "Splendor"));
        itemList.add(new Pair<Long, String>((long) 3, "7 Wonders"));
        itemList.add(new Pair<Long, String>((long) 4, "Codenames"));
        itemList.add(new Pair<Long, String>((long) 5, "Deception"));
    }

    //TODO Options in upper right Corner
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    //Ignore
    @Override
    public void onItemSwipeStarted(ListSwipeItem item) {}
    @Override
    public void onItemSwiping(ListSwipeItem item, float swipedDistanceX) {}

    //Swipe To Delete or Edit
    @Override
    public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {
        Pair<Long, String> itemToGet = (Pair<Long, String>) item.getTag();
        int position = dragListView.getAdapter().getPositionForItem(itemToGet);
        if (swipedDirection == ListSwipeItem.SwipeDirection.LEFT) {
            dragListView.getAdapter().removeItem(position);
            dragListView.getAdapter().notifyDataSetChanged();
        } else if (swipedDirection == ListSwipeItem.SwipeDirection.RIGHT) {
            callDialog(position, itemToGet.second);
        }
    }

    /**
     * @param position list position of item - irrelevant if new
     * @param name current name of item - set to null if new
     */
    private void callDialog(int position, String name) {
        EditDialogFragment dialog = new EditDialogFragment();
        dialog.setListener(this);
        dialog.setPosition(position);
        dialog.setName(name);
        dialog.show(getSupportFragmentManager(), "EditDialogFragment");
    }

    @Override
    public void dialogOk(String name, boolean isNew, int position) {
        if (isNew) {
            itemList.add(itemList.size(), new Pair<Long, String>((long) itemList.size(), name));
        } else {
            itemList.remove(position);
            itemList.add(position, new Pair<Long, String>((long) position, name));
        }
        dragListView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void dialogCanceled() {
        dragListView.getAdapter().notifyDataSetChanged();
    }

    //FAB click
    @Override
    public void onClick(View v) {
        callDialog(0, null);
    }

    //FAB visibility
    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                fab.hide();
            } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                fab.show();
            }
        }
    };

    //For List
    private static class MyDragItem extends DragItem {

        MyDragItem(Context context, int layoutId) {
            super(context, layoutId);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            CharSequence text = ((TextView) clickedView.findViewById(R.id.item_text)).getText();
            ((TextView) dragView.findViewById(R.id.item_text)).setText(text);
            dragView.findViewById(R.id.item_layout).setBackgroundColor(dragView.getResources().getColor(android.R.color.background_light));
        }
    }
}
