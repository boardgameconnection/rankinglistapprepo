package boardgameconnection.de.rankinglists;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

public class DragListAdapter extends DragItemAdapter<Pair<Long, String>, DragListAdapter.ViewHolder> {

    private int layoutId;
    private int grabHandleId;
    private boolean dragOnLong;

    public DragListAdapter(ArrayList<Pair<Long, String>> list,  int layoutId, int grabHandleId, boolean dragOnLong) {
        this.layoutId = layoutId;
        this.grabHandleId = grabHandleId;
        this.dragOnLong = dragOnLong;
        setItemList(list);
    }

    @Override
    public long getUniqueItemId(int position) {
        return mItemList.get(position).first;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        String text = mItemList.get(position).second;
        holder.text.setText(text);
        holder.numberText.setText("" + (position + 1));
        holder.itemView.setTag(mItemList.get(position));
        switch (position) {
            case 0:
                holder.circle.setImageDrawable(holder.circle.getContext().getDrawable(R.drawable.circle_gold));
                break;
            case 1:
                holder.circle.setImageDrawable(holder.circle.getContext().getDrawable(R.drawable.circle_silver));
                break;
            case 2:
                holder.circle.setImageDrawable(holder.circle.getContext().getDrawable(R.drawable.circle_bronze));
                break;
            default:
                holder.circle.setImageDrawable(holder.circle.getContext().getDrawable(R.drawable.circle));
                break;
        }
    }

    public class ViewHolder extends DragItemAdapter.ViewHolder {

        TextView text;
        TextView numberText;
        ImageView circle;

        public ViewHolder(View itemView) {
            super(itemView, grabHandleId, dragOnLong);
            text = itemView.findViewById(R.id.item_text);
            numberText = itemView.findViewById(R.id.item_number);
            circle = itemView.findViewById(R.id.image_circle);
        }


    }
}
