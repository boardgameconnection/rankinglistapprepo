package boardgameconnection.de.rankinglists;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.input.InputManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static android.view.inputmethod.EditorInfo.IME_NULL;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditDialogFragment extends DialogFragment implements View.OnClickListener, DialogInterface.OnClickListener, TextWatcher, TextView.OnEditorActionListener {

    //TODO when clicked outside

    private EditText editTextListItemName;
    private ImageView imageViewClearItemName;
    private DialogInteractionListener listener;
    private String name;
    private int position;
    private AlertDialog dialog;

    public EditDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.fragment_edit_dialog, null);

        setupViews(v);
        return buildDialog(v);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void setupViews(View v) {
        editTextListItemName = v.findViewById(R.id.editText_item_name);
        editTextListItemName.addTextChangedListener(this);
        editTextListItemName.setOnEditorActionListener(this);
        editTextListItemName.requestFocus();
        imageViewClearItemName = v.findViewById(R.id.imageView_clear_item_name);
        if (name != null)
            editTextListItemName.setText(name);
        imageViewClearItemName.setOnClickListener(this);
        if (editTextListItemName.getText().toString().isEmpty())
            imageViewClearItemName.setVisibility(View.GONE);
    }

    private Dialog buildDialog(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        builder.setTitle(R.string.editListItemTitle);
        builder.setPositiveButton(R.string.editListItemAccept, this);
        builder.setNegativeButton(R.string.editListItemCancel, this);
        dialog = builder.create();
        return dialog;
    }

    @Override
    public void onClick(View v) {
        editTextListItemName.setText("");
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            String s = editTextListItemName.getText().toString();
            if (s != null && !s.isEmpty())
                listener.dialogOk(s, name == null, position);
            else
                listener.dialogCanceled();
        } else
            listener.dialogCanceled();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        listener.dialogCanceled();
        super.onDismiss(dialog);
    }

    public EditText getEditTextListItemName() {
        return editTextListItemName;
    }

    public ImageView getImageViewClearItemName() {
        return imageViewClearItemName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setListener(DialogInteractionListener listener) {
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (editTextListItemName.getText().toString().isEmpty())
            imageViewClearItemName.setVisibility(View.GONE);
        else
            imageViewClearItemName.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE && !editTextListItemName.getText().toString().isEmpty()) {
            listener.dialogOk(editTextListItemName.getText().toString(), name == null, position);
            if (name == null)
                editTextListItemName.setText("");
            else
                dismiss();
        }
        return true;
    }

    public interface DialogInteractionListener {
        void dialogOk(String name, boolean isNew, int position);

        void dialogCanceled();
    }
}
